package dev.rodriguez.main;

import dev.rodriguez.controlers.AuthController;
import dev.rodriguez.controlers.BookItemController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.apibuilder.ApiBuilder.post;


public class JavalinApp {

    BookItemController bookItemController = new BookItemController();
    AuthController authController = new AuthController();

    Javalin app = Javalin.create().routes(()->{
        path("books", ()->{
            before("/", authController::authorizeToken);
            get(bookItemController::handleGetAllItemsRequest);
            post(bookItemController::handlePostNewBookItemRequest);
            path(":id", ()->{
                before("/", authController::authorizeToken);
                get(bookItemController::handleGetItemByIDRequest);
                put(bookItemController::handlePutUpdatePriceByIDRequest);
                delete(bookItemController::handleDeleteItemByIDRequest);
            });
        });
        path("login",()->
                post(authController::authenticateLogin)
        );
    });

    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.start();
    }

}
