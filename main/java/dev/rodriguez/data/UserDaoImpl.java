package dev.rodriguez.data;

import dev.rodriguez.model.BookItem;
import dev.rodriguez.model.User;
import dev.rodriguez.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


// the connection object establishes connections with a database to execute SQL statements against it
// in addition, perform other operations such as commits and rollbacks

/**
 * Data Access Object (DAO): JDBC used to connect to database
 */


public class UserDaoImpl implements UserDao {

    private final Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    /**
     *
     * @return boolean
     */
    @Override
    public boolean existsUser(String userName, String pw) {

        try(Connection connection = ConnectionUtil.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement("select count(staff_id) from public.staff where user_name = ? and user_password = ?")) {
            preparedStatement.setString(1, userName);
            preparedStatement.setString(2, pw);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                logger.info("User: " + userName + " exists.");
                return true;
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return false;
    }


    /**
     * v0:
     * crud method: read
     */
    @Override
    public List<User> getAllUsers() {
        List<User> users = new ArrayList<>();

        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from staff");
            ResultSet resultSet = preparedStatement.executeQuery();

            while(resultSet.next()) {
                String userName = resultSet.getString("user_name");
                String email = resultSet.getString("email");
                String pw = resultSet.getString("user_password");
                User user = new User(userName, email, pw);
                users.add(user);
            }
            logger.info("The connections has been successfully established!");
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return users;
    }

    @Override
    public User getUserByUserID(int userID) {
        /**
         * Connection implements Autocloseable
         *      -> try with resources
         */
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * staff where id = ?");
            preparedStatement.setInt(1,userID);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                String userName = resultSet.getString("user_name");
                String email = resultSet.getString("email");
                logger.info("User retrieved from database by id: " + userID);
                return new User(userName, email);
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }

    @Override
    public User getUserByUserName(String userName) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * staff where user_name = ?");
            preparedStatement.setString(1, userName);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                String email = resultSet.getString("email");
                logger.info("User retrieved from database by id: " + userName);
                return new User(userName, email);
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }

    @Override
    public User getUserByEmail(String email) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * staff where email = ?");
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {
                String userName = resultSet.getString("user_name");
                logger.info("User retrieved from database by email: " + email);
                return new User(userName, email);
            }
        } catch (SQLException ex) {
            logger.warn("A connection error has occurred!");
            logger.error(ex.getClass() + " " + ex.getMessage());
        }
        return null;
    }

    /**
     * vn:
     * other crud methods
     */

    public User addNewUser(User user) { return null; }

    /**
     * crud method: delete
     */
    public void deleteUser(int userID) { }

    /**
     * crud method: update
     */
    public void updateUserPassword(int userID, String pw) {}



}
