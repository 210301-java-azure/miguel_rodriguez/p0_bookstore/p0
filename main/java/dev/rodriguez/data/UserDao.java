package dev.rodriguez.data;

import dev.rodriguez.model.BookItem;
import dev.rodriguez.model.User;

import java.util.List;

public interface UserDao {


    boolean existsUser(String userName, String pw);

    /**
     * crud method: read
     */
    List<User> getAllUsers();

    User getUserByUserID(int userID);

    User getUserByUserName(String userName);

    User getUserByEmail(String email);

    /**
     *
     * vn:
     *
     *  User addNewUser(User user);
     *
     */



    /**
     * crud method: create
     */


    /**
     * crud method: delete
     */
    void deleteUser(int userID);

    /**
     * crud method: update
     */
    void updateUserPassword(int userID, String pw);

}
