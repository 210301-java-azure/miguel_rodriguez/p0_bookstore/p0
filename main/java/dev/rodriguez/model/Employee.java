package dev.rodriguez.model;

import java.util.Objects;

public class Employee {
    private String first_name;
    private String middle_name;
    private String last_name;

    private String employeeID;

    private String emailAddress;

    public Employee() {
        this.first_name = null;
        this.middle_name = null;
        this.last_name = null;
        this.employeeID = null;
        this.emailAddress = null;
    }

    public Employee(String first_name, String middle_name, String last_name, String employeeID, String emailAddress) {
        this.first_name = first_name;
        this.middle_name = middle_name;
        this.last_name = last_name;
        this.employeeID = employeeID;
        this.emailAddress = emailAddress;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public void setEmployeeID(String employeeID) {
        this.employeeID = employeeID;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(first_name, employee.first_name) && Objects.equals(middle_name, employee.middle_name) && Objects.equals(last_name, employee.last_name) && Objects.equals(employeeID, employee.employeeID) && Objects.equals(emailAddress, employee.emailAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first_name, middle_name, last_name, employeeID, emailAddress);
    }
}
