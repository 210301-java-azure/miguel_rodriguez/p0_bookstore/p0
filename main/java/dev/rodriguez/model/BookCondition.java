package dev.rodriguez.model;

public enum BookCondition {

    NEW, GOOD, FAIR, POOR

}
