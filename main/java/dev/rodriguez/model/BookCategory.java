package dev.rodriguez.model;

public enum BookCategory {

    FICTION, NONFICTION, HISTORY, PHILOSOPHY, POETRY, BUSINESS, SCIENCE, ART

}
