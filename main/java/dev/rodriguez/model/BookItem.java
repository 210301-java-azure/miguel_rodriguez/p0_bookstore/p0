package dev.rodriguez.model;

import java.io.Serializable;
import java.util.Objects;

public class BookItem implements Serializable {

    /**
     * v0:
     */
    private int bookID;         //ideally, an isbn-13; however, isbn-5
    private String bookTitle;
    private double price;

    private Author author;      //ideally, possible authors; however, one author

    /**
     * v1:
     *
     *     private BookCategory category;
     *     private BookCondition condition;
     *
     */

    public BookItem() {
        super();
        bookID = 0;
        bookTitle = null;
        price = 0.00;
        author = null;
    }

    public BookItem(String bookTitle, double price) {
        this.bookID = 0;
        this.bookTitle = bookTitle;
        this.price = price;
        author = null;
    }

    public BookItem(int bookID, String bookTitle, double price) {
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.price = price;
        author = new Author();
    }

    public BookItem(int bookID, String bookTitle, double price, Author author) {
        this.bookID = bookID;
        this.bookTitle = bookTitle;
        this.price = price;
        this.author = author;
    }

    public int getBookID() {
        return bookID;
    }

    public void setBookID(int bookID) {
        this.bookID = bookID;
    }

    public String getBookTitle() {
        return bookTitle;
    }

    public void setBookTitle(String bookTitle) {
        this.bookTitle = bookTitle;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * vn:
     */
    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookItem bookItem = (BookItem) o;
        return bookID == bookItem.bookID && Double.compare(bookItem.price, price) == 0 && Objects.equals(bookTitle, bookItem.bookTitle) && Objects.equals(author, bookItem.author);
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookID, bookTitle, price, author);
    }

    @Override
    public String toString() {
        return "BookItem{" +
                "bookID=" + bookID +
                ", bookTitle='" + bookTitle + '\'' +
                ", price=" + price +
                ", author=" + author +
                '}';
    }
}
