package dev.rodriguez.controlers;

import dev.rodriguez.model.BookItem;
import dev.rodriguez.services.BookItemService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.javalin.http.Context;

/**
 * Controller: Javalin is used to handle http requests and prepare responses
 */


public class BookItemController {

    private final Logger logger = LoggerFactory.getLogger(BookItemController.class);

    private final BookItemService service = new BookItemService();

    /**
     * GET request
     */
    public void handleGetAllItemsRequest(Context ctx) {
        logger.info("retrieving all records...");
        ctx.json(service.getAllBooks());
        ctx.status(200);
    }

    /**
     * GET request
     */
    public void handleGetItemByIDRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            BookItem item = service.getBookByItemID(idInput);
            System.out.println(item);
            if(item == null) {
                logger.warn("Error: No book found with such id: " + idInput);
                throw new NotFoundResponse("Error: No book found with such id: " + idInput);
            } else {
                logger.info("Success: book found with id: " + idInput);
                ctx.json(item);
            }
        } else {
            throw new BadRequestResponse("Error: {" + idString + "} cannot be parsed.");
        }
    }

    /**
     * POST request
     */
    public void handlePostNewBookItemRequest(Context ctx) {
        BookItem item = ctx.bodyAsClass(BookItem.class);
        logger.info("creating new record...");
        service.addBook(item);
        ctx.status(201);
    }


    /**
     * DELETE request
     */
    public void handleDeleteItemByIDRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            logger.info("Success: book deleted with id: " + idString);
            service.removeBook(idInput);
        } else {
            throw new BadRequestResponse("Error: {" + idString + "} cannot be parsed.");
        }
    }

    /**
     * PUT request
     */
    public void handlePutUpdatePriceByIDRequest(Context ctx) {
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {
            int idInput = Integer.parseInt(idString);
            BookItem item = service.getBookByItemID(idInput);
            if(item == null) {
                logger.warn("Error: No book found with such id: " + idInput);
                throw new NotFoundResponse("Error: No book found with such id: " + idInput);
            } else {
                String bodyString = ctx.body();
                System.out.println("bodyString:" + bodyString);
                String priceString = ctx.formParam("price");
                if (priceString != null) {
//                BookItem item = ctx.bodyAsClass(BookItem.class);
                    /**
                     * Content-type: application/x-www-form-urlencoded
                     */
                    double price = Double.parseDouble(priceString);
                    service.updateBookPrice(idInput,price);
                    logger.info("Success: book's price updated with id: " + idString);
                } else {
                    throw new BadRequestResponse("Error: {" + priceString + "} cannot be parsed.");
                }
            }
        } else {
            throw new BadRequestResponse("Error: {" + idString + "} cannot be parsed.");
        }
    }

}
