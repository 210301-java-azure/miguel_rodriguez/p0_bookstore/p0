package dev.rodriguez.controlers;

import dev.rodriguez.model.User;
import dev.rodriguez.services.BookItemService;
import dev.rodriguez.services.UserService;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuthController {

    private final Logger logger = LoggerFactory.getLogger(AuthController.class);

    private final UserService service = new UserService();

    public void authenticateLogin(Context ctx) {
        /**
         * Content-type: application/x-www-form-urlencoded
         */

        String userString = ctx.formParam("username");
        String pwString = ctx.formParam("password");

        logger.info("{} attempted login",userString);
        if(userString != null && pwString != null) {
            if(userString!=null && userString.equals("postgres")) {
                if(pwString != null && pwString.equals("password")) {
                    logger.info("Successful login!");
                    //send back token
                    ctx.header("Authorization", "admin-auth-token");
                    ctx.status(200);
                    return ;
                }
                throw new UnauthorizedResponse("Error: Incorrect Password");
            }

            /**
             * Invoke service method, checking for user
             */
            if(service.doesUserExist(userString, pwString)) {
                User user = service.getUserByUserName(userString);
                if(pwString.equals(user.getPassword()) && pwString != null) {
                    logger.info("Successful login!");
                    //send back token
                    ctx.header("Authorization", "admin-auth-token");
                    ctx.status(200);
                    return ;
                }
                throw new UnauthorizedResponse("Error: Incorrect Password");
            }
            throw new UnauthorizedResponse("Error: Unrecognized user");
        }


    }

    public void authorizeToken(Context ctx) {
        logger.info("attempting to authorize token...");

        String authHeader = ctx.header("Authorization");

        if(authHeader!=null && authHeader.equals("admin-auth-token")) {
            logger.info("Request authorized. Proceeding to handler...");
        } else {
            logger.warn("Error: Improper authorization.");
            throw new UnauthorizedResponse("Error: Improper authorization.");
        }
    }
}
