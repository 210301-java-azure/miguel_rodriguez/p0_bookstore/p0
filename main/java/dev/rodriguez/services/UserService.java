package dev.rodriguez.services;


import dev.rodriguez.data.UserDao;
import dev.rodriguez.data.UserDaoImpl;
import dev.rodriguez.model.User;

import java.util.List;

/**
 * business logic:
 *
 */

public class UserService {

    private final UserDao userDao = new UserDaoImpl();

    // crud method: read
    public List<User> getAllBooks() {
        return userDao.getAllUsers();
    }

    // crud method: read
    public User getUserByUserID(int userID) {
        return userDao.getUserByUserID(userID);
    }

    // crud method: read
    public User getUserByUserName(String userName) {
        return userDao.getUserByUserName(userName);
    }

    // crud method: read
    public User getUserByEmail(String email) {
        return userDao.getUserByUserName(email);
    }

    public boolean doesUserExist(String userName, String pw) {
        return userDao.existsUser(userName, pw);
    }

}
