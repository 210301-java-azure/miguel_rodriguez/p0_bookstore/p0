<h1>P0: Mesquite Bookstore</h1>

<h2>Introduction</h2>
In this project, I built the backend for my application and set the groundwork for frontend development.
I developed the means of communication between my server and database via JDBC and between our server and 
potential client via Javalin. We find the implementation of Javalin in my Controllers and the implementation 
of JDBC in my Data Access Objects.


<h2>Entity Relational Diagram (ERD) </h2>
<h3>Version 0</h3>
My first entity relational diagram is [Book](./assets/book.png).

My second entity relational diagram is [Staff](./assets/staff.png).

My overall entity relational diagram is [Overall](./assets/bookstore-initial-version.png).

<h3>Version N</h3>
[Author](./assets/author.png) entity relational diagram.

[Store](./assets/store.png) entity relational diagram.

<h4>Associative Tables</h4>
[Book-Author](./assets/book_author.png) entity relational diagram.

<h2>Routes</h2>
<h3>HTTP methods supported:</h3>

<li>GET: /books</li>

<li>GET: /books/1</li>

<li>PUT: /books/1</li>

<li>POST: /books</li>

<li>DELETE: /books/17</li>


<h2>Testing</h2>

<h3>Testing [Via Postman]</h3>
<li>GET Method:	/books</li>

![img_1.png](assets/img_1.png)

<li>GET	Method: /books/5</li>

![img.png](assets/img.png)

<li>POST Method: /books</li>

![img_4.png](assets/img_4.png)

![img_5.png](assets/img_5.png)

<li>DELETE Method: /books/17</li>

![img_2.png](assets/img_2.png)

![img_3.png](assets/img_3.png)

<li>PUT Method:	/books/1</li>

![img_6.png](assets/img_6.png)

![img_7.png](assets/img_7.png)

<h3>Unit Testing</h3>

<h4>FAILED</h4>

<h2>Basic Login Functionality</h2>
<li>Scenario A:</li>

![img_1.png](login_a.png)

<li>Scenario B:</li>

FAIL

<h2>Self-Assessment</h2>
<li>Reach out.</li>

<li>Integrate authentication with JWT.</li>

<li>Integrate testing into future projects.</li>