package dev.rodriguez.controllers;

import dev.rodriguez.controlers.BookItemController;
import dev.rodriguez.model.BookItem;
import dev.rodriguez.services.BookItemService;
import io.javalin.http.Context;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class BookItemControllerTest {

    @InjectMocks
    private BookItemController bookItemController;

    @Mock
    private BookItemService service;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void testGetAllBookItemsRequest(){
        List<BookItem> items = new ArrayList<>();
        items.add(new BookItem(10000, ""));
        Context context = mock(Context.class);
        bookItemController.handleGetAllItemsRequest(context);
        verify(context.json(items));
    }





}
