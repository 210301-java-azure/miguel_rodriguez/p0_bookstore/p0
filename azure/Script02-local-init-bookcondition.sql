-- init book_condition
insert into book_condition (name) values ('NEW');
insert into book_condition (name) values ('GOOD');
insert into book_condition (name) values ('FAIR');
insert into book_condition (name) values ('POOR');